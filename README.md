# Lunduke.Tech

An idea for a static site which points to the online Lunduke properties.

## Gitlab Hosted Preview:
https://gginorio.gitlab.io/lunduke.tech/

## Screenshot: 
![Screenshot](./public/images/screenshot.png "Screenshot")
